angular.module('orgChart.constants', [])
    .constant('Constants', {
        serverPath: '@@serverPath@@',
        devServer: 'https://orgchart-dev.corp.emc.com',
        apiVersion: '/api/orgchart',
        globalError: 'The requested resource is not found. Please contact administrator.',
        networkError: 'Server could not be found. Please check your network connection or contact administrator.',
        timeout: 60000
    })
;