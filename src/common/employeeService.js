angular.module('orgChart.employeeService', [])
    .service('employeeService', function (Restangular, $q, $timeout) {

        this.get = function (employeeId) {
            return Restangular.one('employeecharts', employeeId).get().then(function (result) {
                var employee = _.find(result, {
                    'employeeId': employeeId
                });
                try {
                    if (!employee) {
                        employee = _.find(result, {
                            'employeeId': "012609"
                        });
                    }
                    employee.reporters = _.without(result, employee);

                } catch (ex) {}
                return employee;
            });
        };

        this.search = function (searchText) {
            return Restangular.one('employees', searchText).get();
        };

        this.getTopPerson = function () {
            var deferred = $q.defer();
            var topEmployeeId = '011864';
            var self = this;
            $timeout(function () {
                self.onTopId = topEmployeeId;
                deferred.resolve(topEmployeeId);
            }, 0);
            return deferred.promise;
        };

        this.goTop = function () {
            var self = this;
            return self.getTopPerson().then(function (topEmployeeId) {
                self.onTop = true;
                return self.setEmployee(topEmployeeId);
            });
        };

        this.setEmployee = function (id) {
            var self = this;
            var data = {};
            return self.get(id).then(function (employee) {
                data.employee = employee;
                self.onTop = id === self.onTopId;

                data.reporters = employee.reporters;
                self.data = data;

                $timeout(function () {
                    //$('body').scrollTop(0);
                    $('html, body').animate({
                        scrollTop: 0
                    }, 0);
                }, 0);
            });
        };

    });