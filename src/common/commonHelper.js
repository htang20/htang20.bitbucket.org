var appDirModule = angular.module('orgChart.commonHelper', []);

appDirModule.factory('commonHelper', function ($rootScope) {
    var commonHelper = {};

    commonHelper.baseUrl = "";

    commonHelper.setAddButton = function (broadcastName, name, onClick) {
        $rootScope.$broadcast(broadcastName, {
            name: name,
            onClick: onClick
        });
    };

    commonHelper.setBaseUrl = function (url) {
        commonHelper.baseUrl = url;
    };

    commonHelper.getBaseUrl = function () {
        return commonHelper.baseUrl;
    };

    commonHelper.getTopObject = function (data, number) {
        var arrayObject = [];
        for (var i = 0; i < number; i++) {
            if (angular.isDefined(data[i])) {
                arrayObject.push(data[i]);
            }
        }
        //you can do client-side formatting here
        return arrayObject;
    };

    commonHelper.compareStr = function(a,b){
        a = a || ''; b = b || '';
        if (a < b) { return -1; }
        if (a > b) { return 1; }
        return 0;
    };

    commonHelper.compareDate = function(a,b){
        var minDate = new Date(-8640000000000000);
        console.log(a,b);
        a  = moment(a || minDate).toDate();
        b  = moment(b|| minDate).toDate();
        if (a < b) { return -1; }
        if (a > b) { return 1; }
        return 0;
    };

    return commonHelper;
});

appDirModule.directive('typeaheadKeydownScrollable', function ($rootScope, $timeout) {
     return {
        restrict: 'EA',
        replace: false,
        scope: {asyncselected: '='},
        compile: function (element, $scope) {
            // Little trick to have a working scroll on key press
            element.bind('keydown', function (evt) {
                if(evt.which===40){
                    $timeout(function(){
                        angular.element.find("ul.dropdown-menu > li.active")[0].scrollIntoView(false);
                    });
                }
                else if(evt.which===38){
                     $timeout(function(){
                        angular.element.find("ul.dropdown-menu > li.active")[0].scrollIntoView(false);
                    });
                }
            });
        }
    };
});

appDirModule.filter('nospace', function () {
    return function (value) {
        return (!value) ? '' : value.replace(/ /g, '');
    };
});

var countUnresolvedPromises = 0;