angular.module('orgChart.appHeader', []).directive('appHeader', function () {
    return {
        replace: true,
        restrict: 'E',
        scope: {},
        templateUrl: 'appheader/appheader.tpl.html',
        controller: function ($scope, $rootScope, $window, employeeService, $timeout, $q) {
            var old, oldResult = [];
            var chars = "abcdefghijklmnopqrstuvwxyz'-_,1234567890 ".split('');

            $scope.search = function (val) {

                var newVal = val.split('').filter(function (c) {
                    return _.contains(chars, c.toLowerCase()) ? c : '';
                }).join('');
                $timeout(function () {
                    $scope.asyncSelected = newVal;
                }, 0);

                if (old == newVal) {
                    $scope.asyncSelected = newVal;
                    var deferred = $q.defer();
                    $timeout(function () {
                        deferred.resolve(oldResult);
                    }, 0);

                    return deferred.promise;
                } else {
                    old = newVal;
                    return employeeService.search(newVal)
                        .then(function (result) {
                            var res = result.map(function (it) {
                                return {
                                    firstName: it.employeeName,
                                    lastName: '',
                                    department: it.departmentName,
                                    employeeId: it.employeeId
                                };
                            });
                            $scope.scrollTop();
                            return res;
                        });
                }
            };

            $scope.scrollTop = function () {
                $timeout(function () {
                    var firstChild = $('.dropdown-menu li:nth-child(1)');
                    if(firstChild.length > 0)
                    {
                        $('.dropdown-menu').scrollTop(firstChild.position().top);
                    }
                }, 100);
            };

            $scope.onSelect = function (item) {
                var id = item.employeeId;
                $scope.asyncSelected = '';
                $rootScope.myPromise = employeeService.setEmployee(id);
            };

            $scope.imgClick = function (event) {
                var element = event.target;
                $('.search-input').val('');
                $(element).animate({
                    width: 0
                }, 0);
                $('.mobile-div').animate({
                    width: 'toggle'
                }, 500, function () {
                    var closeIco = $('.closeIco');
                    closeIco.fadeIn();
                    closeIco.css('right', '40px').css('width', 0).css('display', 'block');
                });
            };

            $scope.closeIconClick = function (event) {
                var element = event.target.parentElement;
                $('.search-input').val('');
                $(element).animate({
                    width: 0
                }, 500);
                $(element).fadeOut();
                $('.img-search').animate({
                    width: 22,
                    'margin-right': 20
                }, 500);
                $('.mobile-div').fadeOut();
            };
        }
    };
});