angular.module('orgChart', [
    'orgChart.appHeader',
    'orgChart.home',
    'orgChart.person',
    'orgChart.topOfChart',

    'orgChart.employeeService',
    'orgChart.exceptionHandler',
    'orgChart.appMessage',
    'orgChart.commonHelper',
    'orgChart.constants',
    'orgChart.loadingTemplate',

    //mock backend
    'appMock',

    'templates-app',
    'ui.bootstrap',
    'ui.router',
    'restangular',
    'cgBusy',
    'ngSanitize'
])

.config(function myAppConfig($stateProvider, $urlRouterProvider, RestangularProvider, Constants) {
        $urlRouterProvider.otherwise('/');
        var serverUrl = document.location.hostname == "localhost" ? Constants.devServer : '..';

        RestangularProvider.setBaseUrl(serverUrl + Constants.apiVersion);
        RestangularProvider.setDefaultHttpFields({timeout: Constants.timeout});
    })
.run(function run(Restangular, appMessageSvc, $q, $timeout, Constants, $rootScope) {
        $rootScope.serverUrl = Restangular.configuration.baseUrl;
        Restangular.setErrorInterceptor(function (err, deferred, responseHandler) {
            var errMsg;
            if (err.status === 404) {
                errMsg = Constants.globalError;
            } 
            else if(err.status <= 0){
                errMsg = Constants.networkError;
            }
            else {
                errMsg = err.data ? err.data.message : err.statusText;
            }
            $timeout(function(){
                appMessageSvc.addError(errMsg);
            });
            $q.reject(err);
        });
    })
.value('basePhotosUrl', '/EmpPhotos')
.controller('AppCtrl', function AppCtrl($scope, $rootScope, $q, appMessageSvc, $timeout) {
     
});