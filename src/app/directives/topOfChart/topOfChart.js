angular.module('orgChart.topOfChart', [])
	.directive('topOfChart', function () {
		return {
			replace: true,
			restrict: 'E',
			templateUrl: 'directives/topOfChart/topOfChart.tpl.html',
			controller: function($scope, $rootScope, employeeService) {
				$scope.gotoTop = function() {
					$rootScope.myPromise = employeeService.goTop();
				};

				$scope.$watch(function() {
						return employeeService.onTop;
					},
					function(val) {
						$scope.visible = !val;
					});
			}
		};
	});