angular.module('orgChart.person', ['orgChart.employeeService'])
    .directive('personWidget', function () {
        return {
            replace: true,
            restrict: 'E',
            scope: {
                data: '=',
                showCollapse: '='
            },
            templateUrl: 'person/person.tpl.html',

            controller: function ($scope, $rootScope, employeeService, basePhotosUrl) {
                $scope.collapse = function () {
                    var id = $scope.data.supervisorId;
                    $rootScope.myPromise = employeeService.setEmployee(id);
                };

                $scope.getImg = function (empId) {
                    if (!empId) {
                        return;
                    }
                    return basePhotosUrl + '/' + parseInt(empId, 10) + '.jpg';
                };
            }
        };
    })
    .directive('personMatrix', function () {
        return {
            replace: true,
            restrict: 'E',
            scope: {
                data: '=',
                managerId: '='
            },
            templateUrl: 'person/personMatrix.tpl.html',

            controller: function ($scope, $rootScope, employeeService) {
                $scope.gotoMatrixManager = function (matrixManagerId) {
                    $rootScope.myPromise = employeeService.setEmployee(matrixManagerId);
                };

                $scope.getManagerId = function (data) {
                    return $scope.managerId != data.supervisorId ? data.supervisorId : data.matrixManagerId;
                };

                $scope.getManagerName = function (data) {
                    return $scope.managerId != data.supervisorId ? data.supervisorName : data.matrixManagerName;
                };

            }
        };
    })
    .directive('personExpand', function () {
        return {
            replace: true,
            restrict: 'E',
            scope: {
                data: '=',
                showExpand: '='
            },
            templateUrl: 'person/personExpand.tpl.html',

            controller: function ($scope, $rootScope, employeeService) {
                $scope.expand = function () {
                    var id = $scope.data.employeeId;

                    $rootScope.myPromise = employeeService.setEmployee(id);
                };
            }
        };
    })
    .filter('formatcomma', function () {
        return function (input) {
            if (!input) {
                return;
            }
            return input.replace(',', ', ').replace(',  ', ', ');
        };
    })
    .directive('fallbackSrc', function () {
        return {
            link: function postLink(scope, iElement, iAttrs) {
                iElement.bind('error', function () {
                    angular.element(this).parent().attr('style', "background: url('" + iAttrs.fallbackSrc + "')");
                });
            }
        };
    })

;