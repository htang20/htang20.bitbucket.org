angular.module('orgChart.home', [
	'ui.router',
	'orgChart.person'
])

.config(function config($stateProvider) {
	$stateProvider.state('home', {
		url: '/',
		views: {
			"main": {
				controller: 'HomeCtrl',
				templateUrl: 'home/home.tpl.html'
			}
		}
	});
})

.controller('HomeCtrl', function HomeController($scope, $rootScope, $window, $timeout, employeeService) {
	$scope._ = _;
	$rootScope.myPromise = employeeService.goTop();

	$scope.$watch(function() {
		return employeeService.data;
	}, function(data) {

		$scope.data = data;
        if (data && data.reporters.length > 2) {
            $scope.personRows = _.chunk(data.reporters, 3);
        }
	});

});