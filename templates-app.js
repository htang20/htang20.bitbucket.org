angular.module('templates-app', ['appheader/appheader.tpl.html', 'directives/appMessage/appMessage.tpl.html', 'directives/loadingTemplate/androidTemplate.tpl.html', 'directives/loadingTemplate/desktopTemplate.tpl.html', 'directives/loadingTemplate/iosTemplate.tpl.html', 'directives/topOfChart/topOfChart.tpl.html', 'home/home.tpl.html', 'person/person.tpl.html', 'person/personExpand.tpl.html', 'person/personMatrix.tpl.html']);

angular.module("appheader/appheader.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("appheader/appheader.tpl.html",
    "<div class=\"container-fluid legend-area sticky\" id=\"nav\">\n" +
    "    <script type=\"text/ng-template\" id=\"customTemplate.html\">\n" +
    "        <a>\n" +
    "            <div class=\"type-ah\">\n" +
    "                <span class=\"one\" ng-bind-html=\"match.model.firstName | typeaheadHighlight: query\"></span>\n" +
    "                <span class=\"two\" ng-bind-html=\"' (' + match.model.employeeId + ')' | typeaheadHighlight: query\"></span>\n" +
    "            </div>\n" +
    "        </a>\n" +
    "    </script>\n" +
    "    <div class=\"pull-left pageheader-area-label\"></div>\n" +
    "    <div id=\"sticky\">\n" +
    "        <div class=\"pull-left legend-info\">\n" +
    "            <div class=\"legend-label legend-label-direct\">DIRECT &amp; MATRIX EMPLOYEE</div>\n" +
    "            <div class=\"legend-label legend-label-admin\">ADMIN EMPLOYEE</div>\n" +
    "        </div>\n" +
    "        <div class=\"form-inline\">\n" +
    "            <div class=\"pull-right svg-icon_zoom svg-icon_zoom-dims img-search\" ng-click=\"imgClick($event)\"></div>\n" +
    "            <div class=\"pull-right form-group mobile-div form-inline\" style=\"-webkit-overflow-scrolling: touch;-webkit-transform: translatez(0);\">\n" +
    "                <label class=\"search-label\">SEARCH: </label>\n" +
    "                <input type=\"search\" ng-model=\"asyncSelected\" class=\"search-input\"\n" +
    "                       placeholder=\"Search Name or ID\"\n" +
    "                       typeahead-append-to-body=\"true\"\n" +
    "                       typeahead-template-url=\"customTemplate.html\"\n" +
    "                       typeahead-keydown-scrollable\n" +
    "                       typeahead-items=\"6\"\n" +
    "                       typeahead-min-length=\"3\"\n" +
    "                       typeahead-focus-first=\"true\"\n" +
    "                       typeahead-on-select=\"onSelect($item)\"\n" +
    "                       typeahead-editable=\"true\"\n" +
    "                typeahead=\"name.lastName + ' ' + name.firstName + ' (' + name.employeeId + ')' for name in search($viewValue) | limitTo:4\">\n" +
    "                <a class=\"closeIco hidden-sm hidden-md hidden-lg\" ng-click=\"closeIconClick($event)\">\n" +
    "                    <div class=\"pull-left svg-icon_cross svg-icon_cross-dims\"></div>\n" +
    "                </a>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("directives/appMessage/appMessage.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("directives/appMessage/appMessage.tpl.html",
    "<div>\n" +
    "    <div class=\"alert alert-danger\" role=\"alert\" ng-if=\"appMessageSvc.errorMsgs.length > 0\">\n" +
    "        <button type=\"button\" class=\"close\" ng-click=\"appMessageSvc.clearError()\" aria-label=\"Close\">\n" +
    "            <span aria-hidden=\"true\">&times;</span>\n" +
    "        </button>\n" +
    "        <p ng-repeat=\"e in appMessageSvc.errorMsgs track by $index\"><span class=\"sr-only\"></span>{{e}}</p>\n" +
    "\n" +
    "    </div>\n" +
    "    <div class=\"alert alert-success\" role=\"alert\" ng-if=\"appMessageSvc.successMsgs.length > 0\">\n" +
    "        <button type=\"button\" class=\"close\" ng-click=\"appMessageSvc.clearSuccess()\" aria-label=\"Close\">\n" +
    "            <span aria-hidden=\"true\">&times;</span>\n" +
    "        </button>\n" +
    "        <p ng-repeat=\"e in appMessageSvc.successMsgs track by $index\">\n" +
    "            <span class=\"glyphicon glyphicon-ok-sign\" aria-hidden=\"true\"></span> {{e}}\n" +
    "        </p>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("directives/loadingTemplate/androidTemplate.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("directives/loadingTemplate/androidTemplate.tpl.html",
    "<div class=\"cg-busy-background\"></div>\n" +
    "<div class=\"cg-busy-default-wrapper\">\n" +
    "    <div class=\"cg-busy-mobile-sign\">\n" +
    "\n" +
    "        <div class=\"cg-busy-android-spinner\">\n" +
    "            <img src=\"assets/android_loader.gif\" />\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"cg-busy-android-text ng-binding\">Loading</div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("directives/loadingTemplate/desktopTemplate.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("directives/loadingTemplate/desktopTemplate.tpl.html",
    "<div class=\"cg-busy-background\"></div>\n" +
    "<div class=\"cg-busy-default-wrapper\">\n" +
    "    <div class=\"cg-busy-default-sign\">\n" +
    "\n" +
    "        <div class=\"cg-busy-default-spinner\">\n" +
    "            <div class=\"bar1\"></div>\n" +
    "            <div class=\"bar2\"></div>\n" +
    "            <div class=\"bar3\"></div>\n" +
    "            <div class=\"bar4\"></div>\n" +
    "            <div class=\"bar5\"></div>\n" +
    "            <div class=\"bar6\"></div>\n" +
    "            <div class=\"bar7\"></div>\n" +
    "            <div class=\"bar8\"></div>\n" +
    "            <div class=\"bar9\"></div>\n" +
    "            <div class=\"bar10\"></div>\n" +
    "            <div class=\"bar11\"></div>\n" +
    "            <div class=\"bar12\"></div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"cg-busy-default-text\">Loading</div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("directives/loadingTemplate/iosTemplate.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("directives/loadingTemplate/iosTemplate.tpl.html",
    "<div class=\"cg-busy-background\"></div>\n" +
    "<div class=\"cg-busy-default-wrapper\">\n" +
    "    <div class=\"cg-busy-mobile-sign\">\n" +
    "\n" +
    "        <div class=\"cg-busy-ios-spinner\">\n" +
    "            <img src=\"assets/ios_loader.gif\" />\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"cg-busy-ios-text ng-binding\">Loading</div>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("directives/topOfChart/topOfChart.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("directives/topOfChart/topOfChart.tpl.html",
    "<div ng-show='visible' class='topOfChart' id=\"topOfChart\" ng-click='gotoTop()' data-spy=\"affix\" data-offset-top=\"50\" >\n" +
    "	<div>&#9650;</div>\n" +
    "    <div>Top of Chart</div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("home/home.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("home/home.tpl.html",
    "<div class=\"home\">\n" +
    "    <div class=\"row home-row-left text-center\">\n" +
    "        <div class=\"col-md-4 col-sm-4 col-md-offset-4 col-sm-offset-4 col-xs-12 widget-top\">\n" +
    "            <person-widget data=\"data.employee\" show-collapse=\"data.employee.supervisorId !== data.employee.employeeId\"></person-widget>\n" +
    "            <person-matrix data=\"data.employee\" manager-id=\"data.employee.employeeId\"></person-matrix>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <!--this code is use for display data > 2 -->\n" +
    "    <div class=\"row home-row hidden-xs\" ng-if=\"data.reporters.length > 2\">\n" +
    "        <div ng-repeat=\"persons in personRows\">\n" +
    "            <table class=\"table-container\">\n" +
    "                <tr>\n" +
    "                    <td ng-repeat=\"person in persons\" class=\"td-person\">\n" +
    "                        <person-widget class=\"\" data=\"person\"></person-widget>\n" +
    "                    </td>\n" +
    "                    <td ng-repeat=\"p in _.range(3 - persons.length)\"></td>\n" +
    "                </tr>\n" +
    "                <tr>\n" +
    "                    <td ng-repeat=\"person in persons\" class=\"td-person td-person-bottom\" ng-class=\"{ 'td-person-matrix' : person.matrixManagerId && person.supervisorId !=='000000' }\">\n" +
    "                        <person-matrix data=\"person\" manager-id=\"$parent.data.employee.employeeId\"></person-matrix>\n" +
    "                    </td>\n" +
    "                    <td ng-repeat=\"p in _.range(3 - persons.length)\" style=\"width: 33.33%\"></td>\n" +
    "                </tr>\n" +
    "                <tr>\n" +
    "                    <td ng-repeat=\"person in persons\" class=\"td-person-bottom\">\n" +
    "                        <person-expand data=\"person\" show-expand=\"person.isManager\"></person-expand>\n" +
    "                    </td>\n" +
    "                    <td ng-repeat=\"p in _.range(3 - persons.length)\">&nbsp;</td>\n" +
    "                </tr>\n" +
    "            </table>\n" +
    "            <!--<div>&nbsp;</div>-->\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <!--this code is use for display data === 2 -->\n" +
    "    <div class=\"row home-row hidden-xs\" ng-if=\"data.reporters.length == 2\">\n" +
    "        <table class=\"table-container\">\n" +
    "            <tr>\n" +
    "                <td ng-repeat=\"person in data.reporters\" class=\"td-person column-50\" ng-class=\"{ 'td-2' : $index == 0 }\">\n" +
    "                    <person-widget data=\"person\" ng-class=\"{ 'person-widget-2': $index == 0  }\">\n" +
    "                    </person-widget>\n" +
    "                </td>\n" +
    "            </tr>\n" +
    "            <tr>\n" +
    "                <td ng-repeat=\"person in data.reporters\" class=\"td-person column-50\" ng-class=\"{ 'td-2' : $index == 0 }\">\n" +
    "                    <person-matrix data=\"person\" manager-id=\"$parent.data.employee.employeeId\" ng-class=\"{ 'person-widget-2': $index == 0  }\"></person-matrix>\n" +
    "                </td>\n" +
    "            </tr>\n" +
    "            <tr>\n" +
    "                <td ng-repeat=\"person in data.reporters\" class=\"column-50\">\n" +
    "                    <person-expand data=\"person\" show-expand=\"person.isManager\" ng-class=\" { 'p-expand-2' : $index == 0 }\" style=\"background-color: #FFFFFF\"></person-expand>\n" +
    "                </td>\n" +
    "            </tr>\n" +
    "        </table>\n" +
    "    </div>\n" +
    "\n" +
    "    <!--this code is use for display data === 1 -->\n" +
    "    <div class=\"row home-row-left text-center hidden-xs\" ng-if=\"data.reporters.length == 1\">\n" +
    "        <div class=\"col-md-4 col-sm-4 col-md-offset-4 col-sm-offset-4 col-xs-12 widget-bottom\">\n" +
    "            <table class=\"table-container\">\n" +
    "                <tr>\n" +
    "                    <td class=\"td-person\">\n" +
    "                        <person-widget data=\"data.reporters[0]\"></person-widget>\n" +
    "                    </td>\n" +
    "                </tr>\n" +
    "                <tr>\n" +
    "                    <td class=\"td-person td-person-bottom\">\n" +
    "                        <person-matrix data=\"data.reporters[0]\" manager-id=\"data.employee.employeeId\"></person-matrix>\n" +
    "                    </td>\n" +
    "                </tr>\n" +
    "                <tr>\n" +
    "                    <td class=\"td-person-bottom\">\n" +
    "                        <person-expand data=\"data.reporters[0]\" show-expand=\"data.reporters[0].isManager\"></person-expand>\n" +
    "                    </td>\n" +
    "                </tr>\n" +
    "            </table>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <!--this code is use for display data in small devices-->\n" +
    "    <div class=\"row home-row visible-xs\">\n" +
    "        <div ng-repeat=\"person in data.reporters\">\n" +
    "            <div class=\"col-xs-12 person-small-screen\" ng-class=\"{ 'person-small-screen-first-child' : $index == 0 }\">\n" +
    "                <!--<person-widget class=\"pull-left\" data=\"person\">-->\n" +
    "                <!--</person-widget>-->\n" +
    "                <person-widget class=\"\" data=\"person\">\n" +
    "                </person-widget>\n" +
    "                <person-matrix data=\"person\" manager-id=\"$parent.data.employee.employeeId\"></person-matrix>\n" +
    "                <person-expand data=\"person\" show-expand=\"person.isManager\"></person-expand>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("person/person.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("person/person.tpl.html",
    "<div class=\"person-widget person-widget-customize\" ng-show=\"data\">\n" +
    "    <div ng-if=\"showCollapse\">\n" +
    "        <div class=\"svgTop\">\n" +
    "            <div class=\"svg-circle_arrow_up svg-circle_arrow_up-dims d-arrow top-arrow\" ng-click=\"collapse()\"></div>\n" +
    "            <div>\n" +
    "                <svg height=\"10\" width=\"2\">\n" +
    "                    <line x1=\"0\" y1=\"0\" x2=\"0\" y2=\"200\" class=\"svg-stroke\"></line>\n" +
    "                </svg>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"person-header\" ng-class=\"'person-header-' + data.employeeType\">{{data.departmentName}}</div>\n" +
    "\n" +
    "    <div class=\"person-content\">\n" +
    "\n" +
    "        <div class=\"col-md-6 col-sm-12 col-xs-6 person-img clear-f\" ng-show=\"data.emailAddress !== ' '\">\n" +
    "            <a ng-href=\"{{data.emailAddress? 'mailto:' + data.emailAddress : ''}}\" ng-style=\" { cursor: data.emailAddress? 'pointer': 'default' }\">\n" +
    "                <div ng-style=\" { background: 'url(' + getImg(data.employeeId) + ')' }\" class=\"div-img\">\n" +
    "                    <img style=\"display:none\" ng-src=\"{{getImg(data.employeeId)}}\" fallback-src=\"assets/fallback-img.png\" />\n" +
    "\n" +
    "                    <div class=\"overlay\" ng-if=\"data.emailAddress\">\n" +
    "                        <svg viewBox=\"0 0 100 100\" width=\"117\" height=\"117\" class=\" svg-circle_email_black svg-circle_email_black-dims\"></svg>\n" +
    "                    </div>\n" +
    "                    <div class=\"overlay-icon\" ng-if=\"data.emailAddress\"></div>\n" +
    "                </div>\n" +
    "            </a>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-6 col-sm-12 col-xs-6 person-img clear-f\" ng-show=\"data.emailAddress === ' '\">\n" +
    "            <div ng-style=\" { background: 'url(' + getImg(data.employeeId) + ')' }\" class=\"div-img\">\n" +
    "                <img style=\"display: none\" ng-src=\"{{getImg(data.employeeId)}}\" fallback-src=\"assets/fallback-img.png\" />\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-6 col-sm-12 col-xs-6 person-detail\">\n" +
    "            <div class=\"person-detail-name\">{{data.employeeName | uppercase | formatcomma}}</div>\n" +
    "            <div class=\"person-detail-tel\" ng-if=\"data.externalPhoneNumber\">\n" +
    "                <a href=\"tel:+{{data.externalPhoneNumber | nospace}}\">\n" +
    "                    <div class=\"pull-left svg-icon-phone svg-icon-phone-dims person-mobile-img\"></div>\n" +
    "                </a>&nbsp; {{data.externalPhoneNumber}}\n" +
    "            </div>\n" +
    "            <div class=\"person-detail-email\" ng-hide=\"data.emailAddress === ' '\">\n" +
    "                <a href=\"mailto:{{data.emailAddress}}\">\n" +
    "                    <div class=\"pull-left svg-icon_email svg-icon_email-dims\"></div>\n" +
    "                </a> &nbsp;Email\n" +
    "                <br/>\n" +
    "                <!--<div class=\"vcard-download\">\n" +
    "                    <a href=\"{{$root.serverUrl}}/vcard/{{data.employeeId}}\" target=\"_blank\">\n" +
    "                        <img src=\"assets/vcard_download.png\" />\n" +
    "                    </a>Add to Contacts\n" +
    "                </div>-->\n" +
    "                <!--<a href=\"{{$root.serverUrl}}/vcard/{{data.employeeId}}\" target=\"_blank\">\n" +
    "                    <img class=\"cell-phone-download\" src=\"assets/user_add.png\" />\n" +
    "                </a>-->\n" +
    "            </div>\n" +
    "            <div class=\"person-detail-comp\">{{data.locationName}}</div>\n" +
    "            <div class=\"person-detail-ext\" ng-if=\"data.internalPhoneNumber\">Ext: {{data.internalPhoneNumber}}</div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "</div>");
}]);

angular.module("person/personExpand.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("person/personExpand.tpl.html",
    "<div class=\"svg svgBottom\" ng-if=\"showExpand\">\n" +
    "    <div>\n" +
    "        <svg height=\"10\" width=\"2\">\n" +
    "            <line x1=\"0\" y1=\"0\" x2=\"0\" y2=\"200\" class=\"svg-stroke\"></line>\n" +
    "        </svg>\n" +
    "    </div>\n" +
    "    <div class=\"svg-circle_arrow_down svg-circle_arrow_down-dims d-arrow\" ng-click=\"$parent.expand()\"></div>\n" +
    "</div>");
}]);

angular.module("person/personMatrix.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("person/personMatrix.tpl.html",
    "<div class=\"bottom-panel\">\n" +
    "    <div class=\"person-detail-matrix-bar\"\n" +
    "         ng-class=\"{ 'person-detail-matrix-bar-content': data.matrixManagerId && data.supervisorId !=='000000'}\">\n" +
    "			<span ng-if=\"data.matrixManagerId && data.supervisorId !=='000000'\" class=\"matrix-text\">\n" +
    "			     <span>{{managerId == data.supervisorId? 'Matrix Manager': 'Supervisor' }}</span>\n" +
    "			     :\n" +
    "			     <a href=\"\" ng-click=\"gotoMatrixManager(getManagerId(data))\">{{getManagerName(data)}}</a>\n" +
    "            </span>\n" +
    "    </div>\n" +
    "</div>");
}]);
